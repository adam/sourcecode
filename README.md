# adam

> A simple survey tool

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
# not working at the moment
npm run dev

# builds everything and starts server for production at localhost:3000
npm start

# runs tests
npm run test

# cleans build directory
npm run clean
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
