#!/usr/bin/env node
'use strict';
const fs = require('fs');

let qnum = 0;
let anum = 0;
const privacyNotice = fs.readFileSync('privacynotice.html').toString();
const welcomePageContent = fs.readFileSync('welcomepage.html').toString();

const endpage = fs.readFileSync('endpage.html').toString();

/*
Generator functions
 */

function Answer(type, options) {
  this.data = [
    {
      'id': anum++,
      'type': type,
      'task': options.task,
    }];
  switch (type) {
    case 'freetext':
      this.data[0].rows = options.rows;
      this.data[0].cols = options.cols;
      this.data[0].defaultText = options.defaultText;
      break;
    case 'checkbox':
    //fall-through
    case 'radiobutton':
      this.data[0].orientation = options.orientation;
    //fall-through
    case 'compose':
    //fall-through
    case 'dropdown':
      this.data[0].options = options.options.map(
        x => typeof x === 'string' ? x : x.data[0]);
      break;
    case 'checkbox-matrix':
    //fall-through
    case 'radiobutton-matrix':
      this.data[0].labels = options.labels;
      this.data[0].options = options.options;
      break;
    default:
      throw new Error(`unknown answertype: ${type}`);
  }
}

function Question(questiontext) {
  this.data = {
    'id': qnum++,
    'questiontext': questiontext,
    'answers': [],
  };
  return this;
}

function Questionnaire(title) {
  this.questionnaire = {
    'id': 1337,
    'starttime': Date.now(),
    'endtime': Date.now()+1000*60*60*24*14, // 1000ms * 60s * 60min * 24h * 14Tage
    'surveytitle': title,
    'welcomePage': welcomePageContent,
    'gdprNotice': privacyNotice,
    'endPage': endpage,
    'questionset': [],
  };
  return this;
}

Questionnaire.prototype.addQuestion = function (questiontext) {
  let q = new Question(questiontext);
  this.questionnaire.questionset.push(q.data);
  return this;
};

Questionnaire.prototype.addAnswer = function (type, options) {
  let a = new Answer(type, options);
  for (let i = 0; i < a.data.length; i++)
    this.questionnaire.questionset[this.questionnaire.questionset.length - 1].answers.push(
      a.data[i]);
  return this;
};
Questionnaire.prototype.addSubQuestion = function (text) {
  this.questionnaire.questionset[this.questionnaire.questionset.length - 1].answers.push('</br></br><div style=\'background-color: #1B5E20; color: #FFFFFF; font-size: large; padding: 10px; box-shadow: 2px 2px 2px grey\'>'+text+'</div>');
  return this;
};

Questionnaire.prototype.addTextAnswer = function (text) {
    this.questionnaire.questionset[this.questionnaire.questionset.length - 1].answers.push(text);
    return this;
};

Questionnaire.prototype.toString = function () {
  return JSON.stringify(this.questionnaire);
};
Questionnaire.prototype.print = function () {
  console.log("questionnaire: " + JSON.stringify(this.questionnaire));
};

/*
Case-Study Questionnaire. Constant Data.
 */

exports.questionnaire = new Questionnaire('Selbstorganisation und Informationsgewinnung im Studium')
  .addQuestion('Wo hast du die Nachricht mit dem Link zu dieser Umfrage zuerst gelesen?')
  .addAnswer('radiobutton', {
      task: 'Wähle eine Option aus.',
    options: [
      'eCampus Mail',
      'Mail Client (z.B. Thunderbird, Outlook)',
      'Stud.IP',
      new Answer('compose', {
        options: [
          'Anderes: ',
          new Answer('freetext', {
            rows: 1,
            cols: 20,
          })
        ]
      })
    ],
  })
  .addQuestion('Auf welchem Gerät bearbeitest du gerade diese Umfrage?')
  .addAnswer('radiobutton', {
      task: 'Wähle eine Option aus.',
    options: [
      'Computer/Laptop',
      'Tablet',
      'Smartphone',
      new Answer('compose', {
        options: [
          'Anderes: ',
          new Answer('freetext', {
            rows: 1,
            cols: 20,
          })
        ]
      }),
    ],
  })
  .addQuestion('Was studierst du?')
  .addAnswer('dropdown', {
    task: "Wähle die Fakultät aus, an der du studierst.",
    defaultText: "Fakultät für Agrarwissenschaften",
    options: [
      "Fakultät für Agrarwissenschaften",
      "Fakultät für Biologie und Psychologie",
      "Fakultät für Chemie",
      "Fakultät für Forstwissenschaften und Waldökologie",
      "Fakultät für Geowissenschaften und Geographie",
      "Fakultät für Mathematik und Informatik",
      "Fakultät für Physik",
      "Juristische Fakultät",
      "Sozialwissenschaftliche Fakultät",
      "Wirtschaftswissenschaftliche Fakultät",
      "Philosophische Fakultät",
      "Theologische Fakultät"
    ],
  })
  .addSubQuestion("Bitte nenne deinen Studiengang/deine Studiengänge.")
  .addAnswer('freetext', {
    task: "Fülle das Feld aus.",
    defaultText: "z.B.: Bsc. Angewandte Informatik",
    rows: 4,
    cols: 20,
  })
  .addQuestion('Welches Geschlecht hast du?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['weiblich', 'männlich', 'anderes', 'keine Angabe'],

  })
  .addQuestion('Im wievielten Semester studierst du?')
  .addAnswer('dropdown', {
    task: 'Bitte zähle alle studierten Semester zusammen. Auch von anderen Hochschulen.',
    options: [
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      'mehr als 10'],
  })
  .addQuestion('Wie zufrieden bist du mit deinem Studiengang?')
  .addAnswer('radiobutton-matrix', {
    task: 'Entscheide für jede Aussage, wie sehr du dieser zustimmst.',
    labels: [
      'stimme gar nicht zu',
      'stimme eher nicht zu',
      'stimme eher zu',
      'stimme voll und ganz zu'
    ],
    options: [
      "Die Auswahl an Veranstaltungen ist groß.",
      "Ich habe viel Eigenverantwortung in der Gestaltung meines Studienplans.",
      "Die Veranstaltungsinhalte sind für meine späteren Berufsvorstellungen hilfreich.",
      "Ich bin mit meiner Studienwahl zufrieden.",
      "Die technische Ausstattung der Uni ist schlecht.",
      "Ich kann mich in meinem Studium kreativ entfalten.",
      "Ich habe zu wenig Freizeit."
    ]
  })
  .addQuestion('Wie oft empfiehlst du deinen Studiengang weiter?')
  .addAnswer('radiobutton', {
    task: "Wähle die passende Option aus.",
    orientation: 'inline',
    options: ['nie', 'selten', 'oft', 'sehr oft'],


  })
  .addQuestion('Welche der folgenden Angebote an der Universität sind dir bekannt?')
  .addAnswer('checkbox', {
    task: 'Wähle mindestens eine Option aus.',
    options: [
      'Psychotherapeutischen Ambulanz (PAS)',
      'Psychosoziale Beratung (PSB)',
      'Beschwerdemanagement',
      'Hochschulsport',
      'Zinsloses Sofortdarlehen des AStA',
      'Semesterticket Rückerstattung des AStA',
      'Mensa',
      'Kurse des Studentenwerks',
      'Gleichstellungsbeauftragte deiner Fakultät',
      'Keine'],
  })
  .addSubQuestion('Wie oft empfiehlst du eines dieser Angebote Kommilitoninnen oder Kommilitonen?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['nie', 'selten', 'oft', 'sehr oft'],
  })
  .addSubQuestion('Über welche Wege hast du von diesen Angeboten erfahren?')
  .addAnswer('checkbox', {
    task: "Wähle mindestens eine Option aus.",
    options: [
      'Uni-Websiten',
      'Flyer',
      'Freunde',
      'Orientierungsphase (O-Phase)',
      new Answer('compose', {
        options: [
          'Andere: ',
          new Answer('freetext', {
            rows: 1,
            cols: 30,
          })
        ]
      }),
    ],

  })
  .addQuestion('Welche der Service/Software-Angebote kennst du?')
  .addAnswer('checkbox', {
    task: "Wähle mindestens eine Option aus.",
    options: [
      'ShareLaTeX der GWDG',
      'Rocketchat der GWDG',
      'Etherpad der GWDG',
      'Studipad',
      'GitLab der GWDG',
      'Owncloud der GWDG',
      'VPN-Client der GWDG',
      'Virenscanner (Uni)',
      'Citavi  (Uni)',
      'Office  (Uni)',
      'Statistikprogramme  (Uni)',
      'MSDN Academic Alliance',
      'Keine'],
  })
  .addSubQuestion('Über welche Wege hast du von diesen Angeboten erfahren?')
  .addAnswer('checkbox', {
    task: "Wähle mindestens eine Option aus.",
    options: [
      'Uni-Websiten',
      'GWDG-Websiten',
      'Flyer',
      'Freunde',
      'Orientierungsphase (O-Phase)',
      new Answer('compose', {
        options: [
          'Andere: ',
          new Answer('freetext', {
            rows: 1,
            cols: 30,
          })
        ]
      }),
    ],
  })
  .addSubQuestion('Wie oft empfiehlst du eines dieser Angebote Kommilitoninnen oder Kommilitonen?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['nie', 'selten', 'oft', 'sehr oft'],
  })
  .addQuestion('Wie bekommst du Informationen zu deiner Veranstaltung?')
  .addAnswer('checkbox-matrix', {
    task: 'Die folgenden Fragen beziehen sich auf die Veranstaltung, über die du diesen Fragebogen zugeschickt bekommen hast. Kreuze die Medien an, über die du an die aufgelisteten Materialien und Informationen gelangst.',
    options: ['Ankündigungen der Veranstaltung (Stud.IP)', 'Forum der Veranstaltung (Stud.IP)', 'Ablaufplan der Veranstaltung (Stud.IP)', 'Dateien der Veranstaltung (Stud.IP)', 'Email', 'FlexNow', 'UniVZ', 'Freunde', 'während der Veranstaltung'],
    labels: ['Skript', 'Übungszettel', 'Ausfall eines Termins', 'Zusatztermine', 'Prüfungsanmeldung', 'Prüfungstermin', 'Abgabefristen', 'Prüfungsergebnisse']
  })
  .addQuestion('Welche Lernmaterialien werden dir für die Lehrveranstaltungen zur Verfügung gestellt?')
  .addAnswer('checkbox', {
    task: "Wähle mindestens eine Option aus.",
    options: [
      'Skript zur Vorlesung',
      'Audiomitschnitte der Vorlesung',
      'Videomitschnitte der Vorlesung',
      'Übungszettel',
      'Musterlösungen zu Übungszetteln',
      'Beispielklausur(en)',
      'Lektüreempfehlungen',
      'E-Learning Materialien (z.B. Ilias-Aufgaben oder Youtube-Trainings)',
      'Versuchsmaterial und/oder Laborzugang',
        'Keine',
      new Answer('compose', {
        options: [
          'Andere: ',
          new Answer('freetext', {
            rows: 1,
            cols: 50,
          })
        ]
      }),
    ]
  })
  .addSubQuestion('Sind die Lernmaterialien ausreichend?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein'],
  })


  .addQuestion('Hattest du zu Semesterbeginn schon einmal Probleme auf Stud.IP zuzugreifen?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein'],
  })
  .addSubQuestion('Hattest du zu Semesterbeginn schon einmal Probleme auf FlexNow zuzugreifen?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein'],
  })
  .addQuestion('Wie lang ist deine Klausurenphase? Wähle die passendste Antwort aus.')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    options: ['0 Wochen', '1 Woche', '2 Wochen', '3 Wochen', '4 oder mehr Wochen', 'Keine sinnvolle Angabe möglich'],
  })
  .addSubQuestion('Wieviel Prozent deiner Klausuren schreibst du im Mittel vor der vorlesungsfreien Zeit?')
  .addAnswer('freetext', {
    task: "Nenne einen Wert zwischen 0% und 100%.",
    defaultText: "z.B.: 80%",
    rows: 1,
    cols: 20
  })
  .addQuestion('Kannst du Prüfungen in deinem Studiengang zur Notenverbesserung wiederholen?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein', 'weiß ich nicht'],
  })
  .addSubQuestion('Wenn ja, wie sieht diese Regelung aus. Beschreibe sie.')
  .addAnswer('freetext', {
    task: "Fülle das Textfeld aus.",
    defaultText: "Hier klicken zum Bearbeiten",
    rows: 4,
    cols: 20,
  })
  .addQuestion('Wie viele Credits hast du im letzten Semester gemacht?')
  .addAnswer('radiobutton', {
    task: "Wähle eine Option aus.",
    options: ['weniger als 10', '10-20', '20-30', '30-40', 'mehr als 40'],
  })
  .addSubQuestion('Wie viele Credits planst du, nächstes Semester zu machen?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    options: ['weniger als 10', '10-20', '20-30', '30-40', 'mehr als 40', 'keine Ahnung'],
  })
  .addSubQuestion('Weißt du schon, welche Veranstaltungen du im nächsten Semester besuchen willst?')
  .addAnswer('radiobutton', {
    task: "Wähle eine Option aus.",
    orientation: 'inline',
    options: ['ja', 'nein', 'teilweise']
  })
  .addQuestion('Wie zufrieden bist du mit Stud.IP insgesamt?')
  .addAnswer('radiobutton', {
    task: "Wähle eine Option aus.",
    options: [
      'sehr unzufrieden', 'eher unzufrieden', 'eher zufrieden', 'sehr zufrieden'
    ]
  })
  .addQuestion('Wie viele deiner Veranstaltungen nutzen Stud.IP?')
  .addAnswer('dropdown', {
      task: 'Wähle eine Option aus.',
    options: [
      '0-25%',
      '25-50%',
      '50-75%',
      '75-99%',
      'alle',
      'keine sinnvolle Angabe möglich'],
  })
  .addSubQuestion('Wenn nicht alle Stud.IP nutzen, über welche Wege bekommst du Informationen zu den Veranstaltungen?')
  .addAnswer('freetext', {
    task: "Fülle das Feld aus.",
    defaultText: "Hier klicken zum Bearbeiten. Beschreibe, welche und wie du Informationen bekommst.",
    rows: 4,
    cols: 30
  })
  .addQuestion('Wie häufig benutzt du Stud.IP in der Vorlesungszeit?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    options: [
      'seltener als einmal pro Monat',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich'
    ],
  })
  .addSubQuestion('Wie oft benutzt du Stud.IP in der vorlesungsfreien Zeit?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    options: [
      'seltener als einmal pro Monat',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich'
    ],
  })
  .addQuestion('Wie oft benutzt du folgende allgemeine Funktionen im Stud.IP?')
  .addAnswer('radiobutton-matrix', {
    task: 'Wähle jeweils eine Option aus.',
    cols: 4,
    labels: [
      'noch nie benutzt',
      'seltener als monatlich',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich'
    ],
    options: [
      'Startseite',
      'Nachrichten-Box',
      'Community',
      'Schwarzes Brett'
    ],
  })
  .addSubQuestion('Wie oft benutzt du folgende Funktionen der Veranstaltungsseite im Stud.IP?')
  .addAnswer('radiobutton-matrix', {
    task: 'Wähle jeweils eine Option aus.',
    cols: 4,
    labels: [
      'noch nie benutzt',
      'seltener als monatlich',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich',
    ],
    options: [
      'eigene Farbgruppierung',
      'alles als gelesen markieren',
      'verschiedene Ansichten (z.B. nach Dozenten, ...)',
      'neue Studiengruppe anlegen',
    ],
  })
  .addSubQuestion('Wie oft benutzt du folgende Funktionen des Planers im Stud.IP?')
  .addAnswer('radiobutton-matrix', {
    task: 'Wähle jeweils eine Option aus.',
    cols: 4,
    labels: [
      'noch nie benutzt',
      'seltener als monatlich',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich',
    ],
    options: [
      'Stundenplan',
      'Terminkalender',
      'Ausblenden von Veranstaltungen',
      'Druckansicht',
      'Termine exportieren',
      'Termine importieren',
    ],
  })
  .addQuestion('Wie oft benutzt du folgende Funktionen der Suche im Stud.IP?')
  .addAnswer('radiobutton-matrix', {
    task: 'Wähle jeweils eine Option aus.',
    cols: 4,
    labels: [
      'noch nie benutzt',
      'seltener als monatlich',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich',
    ],
    options: [
      'Veranstaltungssuche',
      'Archiv',
      'Personensuche',
      'Einrichtungssuche',
      'Ressourcensuche',

    ],
  })
  .addSubQuestion('Wie oft benutzt du folgende Funktionen der Tools im Stud.IP?')
  .addAnswer('radiobutton-matrix', {
    task: 'Wähle jeweils eine Option aus.',
    cols: 4,
    labels: [
      'noch nie benutzt',
      'seltener als monatlich',
      'mehrmals pro Monat',
      'mehrmals pro Woche',
      'täglich',
    ],
    options: [
      'Fragebögen',
      'Evaluationen',
      'Literatur',
      'Lernmodule',
      'Export',

    ],
  })
  .addSubQuestion('Kennst du das DoIT-System innerhalb des Stud.IP Systems?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein'],
  })
  .addQuestion('Benutzt du die verschiedenen eCampusdienste nur über eCampus oder greifst du auf manche direkt zu? ')
  .addAnswer('radiobutton', {
    task: ' Gemeint sind Dienste wie Stud,IP, Email, UniVZ, Mensa Essensplan, FlexNow, z.B. über studip.uni-goettingen.de. Wähle eine Option aus.',
    options: ['über eCampus', 'teilweise direkt', 'überwiegend direkt', 'andere Wege'],
  })

  .addQuestion('Auslandssemester')
  .addAnswer('radiobutton', {
    task: 'Hast du schonmal ein Auslandsemester/studienrelevanten Auslandsaufenthalt gemacht?',
    orientation: 'inline',
    options: ['ja', 'nein'],
  })
  .addSubQuestion('Wenn du noch keinen Auslandsaufenthalt gemacht hast, warum nicht?')
  .addAnswer('checkbox', {
    task: "Wähle eine Option aus.",
    options: [
      'Kein Interesse',
      'Ich würde gerne, aber ich weiß nicht, an wen ich mich wenden kann.',
      'Ist schon geplant',
      new Answer('compose', {
        options: [
          'Andere: ',
          new Answer('freetext', {
            rows: 1,
            cols: 20,
          })
        ]
      }),
    ]
  })
  .addSubQuestion('Gibt es in deinem Studiengang eine Informationsveranstaltung zu Auslandaufenthalten?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein', 'weiß ich nicht'],
  })
  .addSubQuestion('Hast du diese schon einmal besucht?')
  .addAnswer('radiobutton', {
    task: 'Wähle eine Option aus.',
    orientation: 'inline',
    options: ['ja', 'nein', 'es gibt keine solche Veranstaltung'],
  })
  .addQuestion('Hast du die Möglichkeit deine Abschlussarbeit in Kooperation mit einer Firma zu schreiben?')
  .addAnswer('radiobutton', {
    task: 'Wähle Zutreffendes aus.',
    orientation: 'inline',
    options: [
      'ja',
      'ja, aber ich wüsste nicht wie',
      ' weiß ich nicht',
      'nein',
    ]
  })
  .addQuestion('Wie nimmst du die Bewerbung von Kolloquien wahr?')
  .addAnswer('radiobutton-matrix', {
    task: 'Gib an, wie sehr diese Aussagen für dich zutreffen.',
    labels: [
      'stimme überhaupt nicht zu',
      'stimme eher nicht zu',
      'stimme eher zu',
      'stimme voll und ganz zu',
      'nicht sinnvoll beantwortbar'
    ],
    options: [
      'Werbung für Kolloquien erreicht mich immer.',
      'Werbung für Kolloquien empfinde ich als Spam.',
      'Ich lese mir durch, worüber ein Kolloquium ist.',
      'Wenn ich Zeit habe, gehe ich immer zu Kolloquien.',
      'Die Bewerbung für Kolloquien ist sehr ansprechend.',
    ]
  })
  .addQuestion('Wie nimmst du das Studienbüro (u.A. Studienberatung) an deiner Fakultät wahr?')
  .addAnswer('checkbox', {
    task: 'Gib an, welche Angebote es gibt.',
    options: [
      'Bachelor-Informationsveranstaltungen',
      'Master-Informationsveranstaltung',
      'Informationsveranstaltung zum Nebenfach/etc.',
      'Auslandssemester-Information',
      'Website des Studienbüros',
      'Informationen zu freien HiWi-Stellen',
      new Answer('compose', {
        options: [
          'Andere: ',
          new Answer('freetext', {
            rows: 1,
            cols: 30,
          })
        ]
      }),
    ]
  })
  .addSubQuestion('Welche Kommunikationskanäle nutzt das Studienbüro?')
  .addAnswer('checkbox', {
    task: 'Wähle Zutreffendes aus.',
    options: [
      'Email',
      'Aushänge',
      'Flyer',
      'Ankündigungen in Lehrveranstaltungen',
      'Fachschaft',
      'Homepage',
      'Blog',
      'Andere',
    ]
  })
  .addSubQuestion('Wie effektiv nutzt das Studienbüro die Kommunikationskanäle?')
  .addAnswer('checkbox-matrix', {
      task: 'Bitte wähle Zutreffendes aus.',
      labels: [
          'Informationen erreichen mich', 'sollte mehr genutzt werden'
      ],
      options: [
          'Email',
          'Aushänge',
          'Flyer',
          'Ankündigungen in Lehrveranstaltungen',
          'Fachschaft',
          'Homepage',
          'Blog',
          'Andere',
      ]
  })

  .addQuestion('Was würde am meisten dazu beitragen, deine Studienqualität zu erhöhen?')
  .addAnswer('freetext', {
      rows: 5,
      cols: 30,
    }
  ).toString();

console.log("questionnaire: " + exports.questionnaire);
