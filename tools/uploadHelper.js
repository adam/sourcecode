let log = console.log;
console.log = function () {};
let questionnaire = JSON.parse(require('./questionnaireGenerator').questionnaire);
const aid = 'gWrjKR3oCyhZWEM032ABsgwk9RIY90';
let axios = require('axios');
axios
    .post(`https://localhost:443/api/questionnaires?aid=${aid}`, questionnaire)
    .then(function (response) {
        if (response.status === 201) {
            log(`${response.data}`);
        } else {
            log('error: ' + response.status);
        }
    });
