#!/usr/bin/env bash
if [ "$EUID" -ne 0 ]; then
    echo "please run as root"
    exit
fi
cd "$(dirname "$0")"
rm -f ../db.json
cd ..
node ./src/server/main.js > /dev/null &
npid=$!
cd tools
# we don't need the frontend code so we don't rebuild it
print "https://localhost/?qid="
sleep 3 && NODE_TLS_REJECT_UNAUTHORIZED=0 node ./uploadHelper.js
sleep 10 && kill $npid

