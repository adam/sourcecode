#!/usr/bin/env node

/**
 * Module dependencies.
 */

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config/config.json'));
const app = require('./app');
const debug = require('debug')('adamserver:server');
const https = require('https');
const log4js = require('log4js');

log4js.configure('config/log4js.json');
const errorLogger = log4js.getLogger('error');
const infoLogger = log4js.getLogger('default');
const apiLogger = log4js.getLogger('api');

let httpsKey;
let httpsCertificate;

try {
    httpsKey = fs.readFileSync("config/keys/privkey.pem");
} catch (err) {
    errorLogger.error("SSL Key missing. Please supply a key (in config/keys/privkey.pem) and a certificate (in config/keys/cert.pem)");
    console.log("SSL Key missing. Please supply a key (in config/keys/privkey.pem) and a certificate (in config/keys/cert.pem)");
}
try {
    httpsCertificate = fs.readFileSync("config/keys/cert.pem");
} catch (err) {
    errorLogger.error("SSL Certificate missing. Please supply a key (in config/keys/privkey.pem) and a certificate (in config/keys/cert.pem)");
    console.log("SSL Certificate missing. Please supply a key (in config/keys/privkey.pem) and a certificate (in config/keys/cert.pem)");
}

try {
    httpsKey = fs.readFileSync("config/keys/privkey.pem");
    httpsCertificate = fs.readFileSync("config/keys/cert.pem");
} catch (err) {
    errorLogger.error("SSL Keys missing. Please supply a key (in config/keys/privkey.pem) and a certificate (in config/keys/cert.pem)");
    console.log("SSL Keys missing. Please supply a key (in config/keys/privkey.pem) and a certificate (in config/keys/cert.pem)");
}

app.set('port', config.httpsPort);


/**
 * Create HTTP server.
 */
const httpsServer = https.createServer({key: httpsKey, cert: httpsCertificate}, app);

/**
 * Listen on provided port, on all network interfaces.
 */
httpsServer.listen(config.httpsPort);
httpsServer.on('error', onError);
httpsServer.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        errorLogger.error(error);
        throw error;
    }

    const bind = typeof config.httpsPort === 'string'
        ? 'Pipe ' + config.httpsPort
        : 'Port ' + config.httpsPort;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            errorLogger.error(bind + ' requires elevated privileges');
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            errorLogger.error(bind + ' is already in use');
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            errorLogger.error(error);
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    const addr = httpsServer.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

/**
 * redirect http to https (if enabled)
 */
let httpServer;
if (config.httpEnabled) {
    if (config.httpRedirect) {
        httpServer = require('http').createServer(function (req, res) {
            try {
                const baseHost = req.headers.host.split(':')[0];
            } catch (err) {
                errorLogger.error(`http-server failed while processing the url "${req.headers.host}". The error was: ${err.toString()}`);
            }
            infoLogger.trace(`redirect from ${req.headers.host} to https://${baseHost}${req.url}`);
            res.writeHead(301, {"Location": `https://${baseHost}${req.url}`});
            res.end();
        });
    } else {
        httpServer = require('http').createServer(app);
    }
    httpServer.listen(config.httpPort);
}

const repl = require("repl").start({prompt: "adam dev console> "});

function addReplContext(repl) {
    repl.context.safe = {};
    repl.context.unsafe = {};
    if (config.httpEnabled)
        repl.context.unsafe.httpServer = httpServer;
    repl.context.unsafe.httpsServer = httpsServer;
    repl.context.unsafe.config = config;
    repl.context.loggers = {
        "errorLogger": log4js.getLogger('error'),
        "infoLogger": log4js.getLogger('default'),
        "apiLogger": log4js.getLogger('api'),
        "dbAndPBLogger": log4js.getLogger('dbAndPb')
    };
    app.addReplContext(repl);

}

addReplContext(repl);
