"user strict";

function isValidID(id_length, id) {
    return id.length === id_length && /^[a-zA-Z0-9]*$/.test(id);
}

function isValidQuestionnaire(id_length, questionnaire) {
    if (typeof(questionnaire) !== "object")
        return false;
    if (!("questionset" in questionnaire && "starttime" in questionnaire && "endtime" in questionnaire))
        return false;
    if ((questionnaire.starttime) !== number || typeof(questionnaire.endtime) !== "number")
        return false;
    if (!isValidQuestionset(questionnaire.questionset))
        return false;
    return true;
}

function isValidQuestionset(questionset) {
    if (typeof(questionset) !== "object")
        return false;
    for (const i in questionset)
        if (!isValidQuestion(questionset[i]))
            return false;
    //all question ids unique
    const questionIDs = questionset.map(q => q.id);
    if (!containsOnlyUniqueElements(questionIDs))
        return false;
    // TODO all answeroption ids unique
    return true;
}

function isValidQuestion(question) {
    if (typeof(question) !== "object")
        return false;
    if (!("id" in question && "questiontext" in question && "answers" in question))
        return false;
    if (typeof(question.id) !== "number" || typeof(question.questiontext) !== "string" || typeof(question.answers) !== "object")
        return false;
    // TODO optional property question.title
    for (const i in question.answers)
        if (!isValidAnswerOption(question.answers[i]))
            return false;
    return true;
}

function isValidAnswerOption(answerOption) {
    if (typeof(answerOption) === "string")
        return true;
    else if (typeof(answerOption) !== "object")
        return false;
    if (!("id" in answerOption && "type" in answerOption))
        return false;
    if (typeof(answerOption.id) !== number || typeof(answerOption.type) !== "string")
        return false;
    // TODO optional property answerOption.task

    switch (answerOption.type) {
        case "freetext":
            // TODO optional property answerOption.defaultText
            return true;
        case "dropdown":
            if (!("options" in answerOption) || typeof(answerOption.options) !== "object")
                return false;
            for (const i in answerOption.options)
                if (typeof(answerOption.options[i]) !== "string")
                    return false;
            return true;
        case "radiobutton":
        case "checkbox":
        case "compose":
            if (!("options" in answerOption) || typeof(answerOption.options) !== "object")
                return false;
            for (const i in answerOption.options)
                if (!isValidAnswerOption(answerOption.options[i]))
                    return false;
            return true;
        default:
            return false;
    }
}

function containsOnlyUniqueElements(array) {
    for (const i in array) {
        if (array.index(array[i]) !== i)
            return false;
    }
    return true;
}

module.exports = {};
module.exports.isValidID = isValidID;
module.exports.isValidQuestionnaire = isValidQuestionnaire;