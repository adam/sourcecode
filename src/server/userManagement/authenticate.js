const fs = require('fs');

const users = JSON.parse(fs.readFileSync('./config/users.conf.json').toString());

function isAdmin(token) {
    return !!users.find(user => user.type === 'admin' && user.id === token);
}

module.exports = {isAdmin};