"use strict";

const express = require('express');
const loki = require('lokijs');
const isValidID = require("../validate-input").isValidID;
const authenticate = require('../userManagement/authenticate.js');
const log4js = require('log4js');
const fs = require("fs");
const path = require('path');

const lokiCryptedFileAdapter = require('../database/lokiCryptedFileAdapter');
lokiCryptedFileAdapter.setSecret(fs.readFileSync(path.join(__dirname, '../../../config/dbKey.secret')).toString());

log4js.configure('config/log4js.json');
const errorLogger = log4js.getLogger('error');
const infoLogger = log4js.getLogger('default');
const dbAndPBLogger = log4js.getLogger('dbAndPb');

let db = new loki('./db.json', {
    autoload: true,
    autoloadCallback: databaseInitialize,
    adapter: lokiCryptedFileAdapter,
    autosave: true,
    autosaveInterval: 4000
});
let router = express.Router();

/**
 * user Collection format: [Userdescription]
 * Userdescription : { qid: string, id: string, hasCompletedSurvey: boolean }
 */
let databaseInitialized = false;
let collections = {};

function databaseInitialize(err) {
    if (err && err instanceof Error) {
        errorLogger.error(err.message);
    } else {
        collections.questionnaires = db.addCollection('questionnaires');
        collections.answers = db.addCollection('answers');
        collections.users = db.addCollection('users');
        collections.accessTimes = db.addCollection('accessTimes');
        infoLogger.info("Database ready.");
        dbAndPBLogger.info("Database ready.");
        databaseInitialized = true;
    }
}


router.use(function (req, res, next) {
    if (databaseInitialized) {
        next();
    } else {
        res.sendStatus(503);
    }
});

const ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
const ID_LENGTH = 30;

const is = require('is_js');
is.validID = (id) => is.truthy(/^[a-zA-Z0-9]{30}$/.test(id));

function generateID() {
    return Array(ID_LENGTH).fill(0).map(() => ALPHABET[Math.floor(Math.random() * ALPHABET.length)]).join('');
}

let progressbarCounter = 0;
let globalProgressbarcounter = [0, 0, 0, 0, 0];

function getNewProgressbarForUser() {
    progressbarCounter = (progressbarCounter + 1) % 5;
    globalProgressbarcounter[progressbarCounter]++;

    switch (progressbarCounter) {
        case 0:
            return 'none';
        case 1:
            return 'linear';
        case 2:
            return 'fast-to-slow';
        case 3:
            return 'slow-to-fast';
        case 4:
            return 'smart';
        default:
            let error = new error('invalid progressbar type');
            errorLogger.error(error);
            throw  error;
    }
}

/* get questionnaire content */
router.get('/questionnaires/:qid', function (req, res) { //?uid=TOKEN
    const qid = req.params.qid;
    const uid = req.query.uid;

    // input validation
    if (!isValidID(ID_LENGTH, qid) || !isValidID(ID_LENGTH, uid) || collections.answers.findOne({
            uid: uid,
            qid: qid
        }) == null || !isQuestionnaireOpen(qid)) {
        res.sendStatus(404);
        return;
    }

    let questionnaire = collections.questionnaires.findOne({id: qid});
    // not found if no object under this qid or the questionnaire has not enough pages
    if (questionnaire === null) {
        res.sendStatus(404);
    } else {
        questionnaire = JSON.parse(JSON.stringify(questionnaire));
        questionnaire.$loki = undefined;
        questionnaire.meta = undefined;
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(questionnaire));
    }
});
/* create new questionnaire */
router.post('/questionnaires', function (req, res) { //?aid=TOKEN

    const aid = req.query.aid;
    if (!authenticate.isAdmin(aid)) {
        res.sendStatus(403);
        return;
    }
    const questionnaire = req.body;
    // TODO input validation
    // validate against questionnaire format specification
    let qid;
    // regenerate id until it is unique
    do {
        qid = generateID();
    } while (collections.questionnaires.findOne({id: qid}) !== null);

    questionnaire.id = qid;
    collections.questionnaires.insert(questionnaire);
    res.setHeader('Content-Type', 'text/plain');
    res.status(201);
    res.send(qid);
});
/* GET answers for given questionnaire */
router.get('/questionnaires/:qid/answers', function (req, res) { //?uid=TOKEN

    const qid = req.params.qid;
    const uid = req.query.uid;

    // input validation
    if (!is.validID(qid) || !is.validID(uid)) {
        res.sendStatus(400);
        return;
    }

    let answers = collections.answers.findOne({qid: qid, uid: uid});

    if (answers === null || !isQuestionnaireOpen(qid)) {
        res.sendStatus(404);
    } else {
        answers = JSON.parse(JSON.stringify(answers));
        answers.meta = undefined;
        answers.$loki = undefined;
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(answers));
    }
});
/* update answers for given questionnaire; overwrites */
router.post('/questionnaires/:qid/answers', function (req, res) { //?uid=TOKEN
    const qid = req.params.qid;
    const uid = req.query.uid;
    // input validation
    if (!is.validID(qid) || !is.validID(uid)) {
        res.sendStatus(400);
        return;
    }
    const answers = collections.answers.findOne({qid: qid, uid: uid});
    if (answers === null || !isQuestionnaireOpen(qid)) {
        res.sendStatus(404);
        return;
    } else {
        // check if body is really json
        answers.answerset = req.body.answerset;
        collections.answers.update(answers);

        const time = collections.accessTimes.findOne({qid: qid, uid: uid});
        const now = Date.now();

        if (time === null) {
            collections.accessTimes.insert({qid: qid, uid: uid, start: now, end: now});
        }
        else {
            time.end = now;
            collections.accessTimes.update(time);
        }

        res.sendStatus(200);
    }
});
/* GET new user token */
router.get('/questionnaires/:qid/users/token', function (req, res) {
    // TODO input validation
    const qid = req.params.qid;
    if (collections.questionnaires.findOne({id: qid}) === null || !isQuestionnaireOpen(qid)) {
        res.sendStatus(404);
        return;
    }

    let uid;
    do {
        uid = generateID();
    } while (collections.users.findOne({qid: qid, id: uid}) !== null);
    collections.users.insert({qid: qid, id: uid});
    const emptyAnswerSet = {
        qid: qid,
        uid: uid,
        answerset: [],
        progressbarType: getNewProgressbarForUser()
    };
    collections.answers.insert(emptyAnswerSet);
    res.setHeader('Content-Type', 'text/plain');
    res.status(201);
    res.send(uid);
});

/* GET progressbar for given questionnaire */
router.get('/questionnaires/:qid/progressbar', function (req, res) { //?uid=TOKEN

    const qid = req.params.qid;
    const uid = req.query.uid;

    // input validation
    if (!is.validID(qid) || !is.validID(uid)) {
        res.sendStatus(400);
        return;
    }

    const answerset = collections.answers.findOne({qid: qid, uid: uid});

    if (answerset === null || !isQuestionnaireOpen(qid)) {
        res.sendStatus(404);
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({progressbarType: answerset.progressbarType}));
    }
});

//store the database size and the number of issued progressbars every hour in a log file

function log() {
    if (databaseInitialized)
        dbAndPBLogger.info(`Database size: ${getDBSizeInMB()} Mb and issued Progressbars: ${globalProgressbarcounter}`);
    else
        dbAndPBLogger.info('Database is not initialized');
};
const hourInMilliseconds = 3600000;
setInterval(log, hourInMilliseconds);

function getDBSizeInMB() {
    const stats = fs.statSync(path.join(__dirname, '../../../db.json'));
    const fileSizeInBytes = stats.size;
    return fileSizeInBytes / 1000000.0;
}

function isQuestionnaireOpen(qid) {
    const questionnaire = collections.questionnaires.findOne({id: qid});
    return Date.now() < questionnaire.endtime;
}

function addReplContext(repl) {
    repl.context.safe.getDBSizeInMB = getDBSizeInMB;
    repl.context.safe.getDatabaseInitialized = () => databaseInitialized;

    repl.context.safe.getAllQids = function () {
        return collections.questionnaires.find({}).map(x => x.id);
    };
    repl.context.safe.getDatabaseInitialized = function () {
        return databaseInitialized;
    };
    repl.context.safe.getProgressbarCounter = function () {
        return progressbarCounter;
    };
    repl.context.safe.getGlobalProgressbarCounter = function () {
        return globalProgressbarcounter;
    };
    repl.context.safe.countQuestionnaires = function () {
        return collections.questionnaires.find().length;
    };
    repl.context.safe.countUsers = function () {
        return collections.users.find().length;
    };
    repl.context.safe.countAnswers = function () {
        return collections.answers.find().length;
    };
    repl.context.safe.getAnswersAsJSON = function () {
        return JSON.stringify(collections.answers.find());
    };
    repl.context.safe.getAnswersForQidAsJSON = function (qid) {
        return JSON.stringify(collections.answers.find({"qid": qid}));
    };
    repl.context.safe.getAnswersForQidForUidAsJSON = function (qid, uid) {
        return JSON.stringify(collections.answers.find({"qid": qid, "uid": uid}));
    };
    repl.context.safe.getLatestChange = function () {
        let times = collections.accessTimes.find().map(x => x.end).sort();
        let lastTime = times[times.length-1];
        let difference = Date.now()-lastTime;
        console.log(`${Math.floor(difference/1000/60/60/24)} Tage, ${Math.floor(difference/1000/60/60)%24} Stunden, ${Math.floor(difference/1000/60)%60} Minuten, ${Math.floor(difference/1000)%60} Sekunden, `)
    };
    repl.context.unsafe.databaseInitialized = databaseInitialized;
    repl.context.unsafe.db = db;
    repl.context.unsafe.collections = collections;
    repl.context.unsafe.progressbarCounter = progressbarCounter;
    repl.context.unsafe.globalProgressbarcounter = globalProgressbarcounter;
    repl.context.unsafe.authenticate = authenticate;
}

module.exports = router;
module.exports.addReplContext = addReplContext;
