// main.js
import 'material-design-icons-iconfont/dist/material-design-icons.css'; // Ensure you are using
// css-loader
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import App from './App.vue';
import answertype from './pagecontent/answertypes/AnswerType';

const axios = require('axios');

Vue.component("answertype", answertype);
window.lang = require('./languages/lang-de');
Vue.use(Vuetify, {
    iconfont: 'mdi', // 'md' || 'mdi' || 'fa' || 'fa4'
    theme: {
        primary: '#4CAF50', //green
        secondary: '#E8F5E9', //green lighten-5
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
    }
});

let getGetParams = function (param) {
    let url = location.search;
    let params = {};
    let query = url.substr(1);
    let vars = query.split('&').filter((s) => s !== '');
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params[param];
};

// try to get params from url
let qid = getGetParams('qid');
let uid = getGetParams('uid');
console.log(`qid: ${qid}; uid: ${uid};`);

// if we have no qid in the url we try to find it in the cache
/*if (qid === undefined) {
    if (localStorage['lastquestionnaire'] !== undefined) {
        location.href = localStorage['lastquestionnaire'];
    }
} else {
    localStorage['lastquestionnaire'] = location.href;
}*/
/*
if (typeof qid === 'string' && uid !== undefined) {
    console.log(`store uid`);
    localStorage[`qid${qid}`] = uid;
}

if (typeof qid === 'string' && uid === undefined) {
    console.log(`load uid`);
    uid = localStorage[`qid${qid}`];
    location.search += `&qid=${qid}`
    location.reload();
}
*/

// if that fails we try local cache
// if (uid === undefined && cacheData) {
//     history.replaceState(null, null, document.URL + `&uid=${uid}`);
//     location.reload();
// }

// if that fails we ask for a new user token
    if (uid === undefined) {
        document.getElementById('app').innerText = "loading...";
        axios.get(`https://${window.location.host}/api/questionnaires/${qid}/users/token`).then((response) => {
            uid = response.data;
            console.log(`qid ${qid}; uid ${uid}`);
//        localStorage[qid] = uid;
            // change history and thereby alter url
            history.replaceState(null, null, document.URL + `&uid=${uid}`);
            location.reload();
        });
    } else {
        history.pushState(null, null, document.URL);
        /* if back button is clicked the last page from the history (added above) is consumed so we re-add it */
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
        new Vue({
            el: '#app',
            render: h => h(App)
        });
    }
