"use strict";
const frisby = require('frisby');
const Joi = frisby.Joi;
const jasmine = require('jasmine');
const axios = require('axios');
const HTTPSPORT = 443;
const UID_LENGTH = 30;
const ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

// start server
//require("../src/server/main.js");

// helper functions for the different requests
function postQuestionnaire(questionnaire, aid) {
    return frisby.post(`https://localhost:${HTTPSPORT}/api/questionnaires?aid=${aid}`, questionnaire);
}

function getAuthenticationToken(qid) {
    return frisby.get(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}/users/token`);
}

function getQuestionnaire(qid, uid) {
    return frisby.get(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}?uid=${uid}`);
}

function getAnswers(qid, uid) {
    return frisby.get(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}/answers?uid=${uid}`);
}

function postAnswers(qid, uid, answers) {
    return frisby
        .post(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}/answers?uid=${uid}`, answers);
}

function getProgressbarType(qid, uid) {
    return frisby.get(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}/progressbar?uid=${uid}`);
}

const AdminToken = 'gWrjKR3oCyhZWEM032ABsgwk9RIY90';

const exampleQuestionnaire = {
    "id": 1337,
    "starttime": 1528358766,
    "endtime": Date.now() + 1000 * 60 * 60 * 24,
    "surveytitle": "Selbstorganisation und Informationsgewinnung im Studium",
    "welcomePage": "<div\n                type=\"first\"\n\n        >\n        </div>\n        <div type=\"second\">\n            <p>\n                <strong>Hallo!</strong>\n            <br/>\n            <br/>\n                Wir sind die studentische Arbeitsgruppe <strong>Studienqualitätsverbesserung </strong>, die sich für deine Studiensituation interessiert.\n                <br/>\n                Mit nur <strong>10 Minuten</strong> deiner Zeit kannst du uns schon helfen, deine Studienqualität zu verbessern!\n                Es wäre klasse, wenn du unseren kurzen annonymen Fragebogen ausfüllen könntest.\n            </p>\n        </div>",
    "gdprNotice": "currently no data is stored permanently",
    "endPage": "So long and thanks for all the fish",
    "questionset": [{
        "id": 0,
        "questiontext": "Wo hast du die Nachricht mit dem Link zu dieser Umfrage zuerst gelesen?",
        "answers": [{
            "id": 2,
            "type": "radiobutton",
            "options": ["Ecampus Mail", "Mail Client (z.B. Thunderbird, Outlook)", "StudIP", {
                "id": 1,
                "type": "compose",
                "options": ["Anderes: ", {"id": 0, "type": "freetext", "rows": 1, "cols": 20}]
            }]
        }]
    }, {
        "id": 1,
        "questiontext": "Auf welchem Gerät bearbeitest du gerade diese Umfrage?",
        "answers": [{
            "id": 5,
            "type": "radiobutton",
            "options": ["Computer/Laptop", "Tablet", "Smartphone", {
                "id": 4,
                "type": "compose",
                "options": ["Anderes: ", {"id": 3, "type": "freetext", "rows": 1, "cols": 20}]
            }]
        }]
    }, {
        "id": 2,
        "questiontext": "Was studierst du?",
        "answers": [{
            "id": 6,
            "type": "dropdown",
            "task": "Wähle die Fakultät aus, an der du studierst.",
            "options": ["Fakultät für Agrarwissenschaften", "Fakultät für Biologie und Psychologie", "Fakultät für Chemie", "Fakultät für Forstwissenschaften und Waldökologie", "Fakultät für Geowissenschaften und Geographie", "Fakultät für Mathematik und Informatik", "Fakultät für Physik", "Juristische Fakultät", "Sozialwissenschaftliche Fakultät", "Wirtschaftswissenschaftliche Fakultät", "Philosophische Fakultät", "Theologische Fakultät"]
        }, {
            "id": 7,
            "type": "freetext",
            "task": "Bitte nenne deinen Studiengang/deine Studiengänge.",
            "rows": 4,
            "cols": 20
        }]
    }, {
        "id": 3,
        "questiontext": "Welches Geschlecht hast du?",
        "answers": [{
            "id": 8,
            "type": "radiobutton",
            "task": "Wähle eine Option aus",
            "orientation": "inline",
            "options": ["weiblich", "männlich", "sonstiges", "keine Angabe"]
        }]
    }, {
        "id": 4,
        "questiontext": "Im wievielten Semester studierst du?",
        "answers": [{
            "id": 9,
            "type": "dropdown",
            "task": "Bitte zähle alle studierten Semester zusammen. Auch von anderen Hochschulen.",
            "options": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "mehr als 10"]
        }]
    }, {
        "id": 5,
        "questiontext": "Wie zufrieden bist du mit deinem Studiengang?",
        "answers": [{
            "id": 10,
            "type": "radiobutton-matrix",
            "task": "Entscheide für jede Aussage, wie sehr du dieser zustimmst.",
            "labels": ["stimme voll und ganz zu", "stimme eher zu", "stimme eher nicht zu", "stimme gar nicht zu"],
            "options": ["Die Auswahl an Veranstaltungen ist groß.", "Ich habe viel Eigenverantwortung in der Gestaltung meines Studienplans.", "Die Veranstaltungsinhalte sind für meine späteren Berufsvorstellungen hilfreich.", "Ich bin mit meiner Studienwahl zufrieden.", "Die technische Ausstattung der Uni ist schlecht.", "Ich kann mich in meinem Studium kreativ entfalten.", "Ich habe zu wenig Freizeit."]
        }]
    }, {
        "id": 6,
        "questiontext": "Wie oft empfiehlst du deinen Studiengang weiter?",
        "answers": [{
            "id": 11,
            "type": "radiobutton",
            "task": "Wähle die passende Option aus.",
            "orientation": "inline",
            "options": ["nie", "selten", "oft", "sehr oft"]
        }]
    }, {
        "id": 7,
        "questiontext": "Angebote der Universität für Studierende",
        "answers": [{
            "id": 12,
            "type": "checkbox",
            "task": "Welche der folgenden Angebote an der Universität sind dir bekannt?",
            "options": ["Psychotherapeutischen Ambulanz (PAS)", "Psychosoziale Beratung (PSB)", "Beschwerdemanagement", "Hochschulsport", "Zinsloses Sofortdarlehen des AStA", "Semesterticket Rückerstattung des AStA", "Gleichstellungsbeauftragte deiner Fakultät"]
        }, {
            "id": 13,
            "type": "radiobutton",
            "task": "Wie oft empfiehlst du eines dieser Angebote Kommilitoninnen oder Kommilitonen?",
            "orientation": "inline",
            "options": ["nie", "selten", "oft", "sehr oft"]
        }, {
            "id": 16,
            "type": "checkbox",
            "task": "Über welche Wege hast du von diesen Angeboten erfahren?",
            "options": ["Uni-Websiten", "Flyer", "Freunde", "Orientierungsphase/O-Phase", {
                "id": 15,
                "type": "compose",
                "options": ["Andere: ", {"id": 14, "type": "freetext", "rows": 1, "cols": 30}]
            }]
        }]
    }, {
        "id": 8,
        "questiontext": "Welche der Service/Software-Angebote kennst du?",
        "answers": [{
            "id": 17,
            "type": "checkbox",
            "task": "Wähle die Angebote aus, die dir bekannt sind.",
            "options": ["ShareLaTeX der GWDG", "Rocketchat der GWDG", "Etherpad der GWDG", "Studipad", "GitLab der GWDG", "Owncloud der GWDG", "Virenscanner (Uni)", "Citavi  (Uni)", "Office  (Uni)", "Statistikprogramme  (Uni)", "MSDN Academic Alliance"]
        }, {
            "id": 20,
            "type": "checkbox",
            "task": "Über welche Wege hast du von diesen Angeboten erfahren?",
            "options": ["Uni-Websiten", "GWDG-Websiten", "Flyer", "Freunde", "Orientierungsphase/O-Phase", {
                "id": 19,
                "type": "compose",
                "options": ["Andere: ", {"id": 18, "type": "freetext", "rows": 1, "cols": 30}]
            }]
        }, {
            "id": 21,
            "type": "radiobutton",
            "task": "Wie oft empfiehlst du eines dieser Angebote Kommilitoninnen oder Kommilitonen?",
            "orientation": "inline",
            "options": ["nie", "selten", "oft", "sehr oft"]
        }]
    }, {
        "id": 9,
        "questiontext": "Wie bekommst du Informationen zu deiner Veranstaltung?",
        "answers": ["Die folgenden Fragen beziehen sich auf die Veranstaltung, über die du diesen Fragebogen zugeschickt bekommen hast.", {
            "id": 22,
            "type": "checkbox-matrix",
            "task": "Kreuze die Medien an, über die du an die aufgelisteten Materialien und Informationen gelangst.",
            "labels": ["Email", "Ankündigungen der Veranstaltung", "Forum der Veranstaltung", "Ablaufplan der Veranstaltung", "Dateien der Veranstaltung", "FlexNow", "UniVZ", "Freunde", "während der Veranstaltung", "schon regelmäßig verpasst"],
            "options": ["Skript", "Übungszettel", "Ausfall eines Termins", "Zusatztermine", "Prüfungsanmeldung", "Prüfungstermin", "Abgabefristen", "Prüfungsergebnisse"]
        }]
    }, {
        "id": 10,
        "questiontext": "Welche Lernmaterialien werden dir für die Lehrveranstaltungen zur Verfügung gestellt?",
        "answers": [{
            "id": 25,
            "type": "checkbox",
            "task": "Wähle aus, welche Materialien dir zur Verfügung stehen.",
            "options": ["Skript zur Vorlesung", "Audiomitschnitte der Vorlesung", "Videomitschnitte der Vorlesung", "Übungszettel", "Musterlösungen zu Übungszetteln", "Beispielklausur(en)", "Lektüreempfehlungen", "E-Learning Materialien (z.B. Ilias-Aufgaben oder Youtube-Trainings)", "Versuchsmaterial und/oder Laborzugang", {
                "id": 24,
                "type": "compose",
                "options": ["Andere: ", {"id": 23, "type": "freetext", "rows": 1, "cols": 50}]
            }]
        }, {
            "id": 26,
            "type": "freetext",
            "task": "Sind die Lernmaterialien ausreichend?",
            "rows": 4,
            "cols": 20
        }]
    }, {
        "id": 11,
        "questiontext": "Aufbau deines Studiengangs allgemein",
        "answers": ["Im Folgenden wollen wir wissen, wie gut du dich in deinem Studiengang auskennst. Welche Möglichkeiten stehen dir offen? Wohin kannst du dich wenden? Woher bekommst du Informationen zu neuen Veranstaltungen oder Änderungen deiner Prüfungsordnung?"]
    }, {
        "id": 12,
        "questiontext": "Probleme zum Semesterstart",
        "answers": [{
            "id": 27,
            "type": "radiobutton",
            "task": "Hattest du zu Semesterbeginn schon einmal Probleme auf StudIP zuzugreifen?",
            "orientation": "inline",
            "options": ["nein", "ja"]
        }, {
            "id": 28,
            "type": "radiobutton",
            "task": "Hattest du zu Semesterbeginn schon einmal Probleme auf FlexNow zuzugreifen?",
            "orientation": "inline",
            "options": ["nein", "ja"]
        }]
    }, {
        "id": 13,
        "questiontext": "Klausuren und Klausurenphase",
        "answers": [{
            "id": 29,
            "type": "radiobutton",
            "task": "Über wie viele Wochen erstreckt sich deine Klausurenphase? Wähle die passendste Antwort aus.",
            "options": ["0", "1", "2", "3", "4 oder mehr"]
        }, {
            "id": 30,
            "type": "freetext",
            "task": "Wieviel Prozent deiner Klausuren schreibst du im Mittel vor der vorlesungsfreien Zeit?",
            "rows": 1,
            "cols": 20
        }]
    }, {
        "id": 14,
        "questiontext": "Kannst du Prüfungen in deinem Studiengang zur Notenverbesserung wiederholen?",
        "answers": [{
            "id": 31,
            "type": "radiobutton",
            "orientation": "inline",
            "options": ["ja", "nein", "weiß ich nicht"]
        }, {
            "id": 32,
            "type": "freetext",
            "task": "Wenn ja, wie sieht diese Regelung aus. Beschreibe sie.",
            "rows": 4,
            "cols": 20
        }]
    }, {
        "id": 15,
        "questiontext": "Wie sieht es bei dir mit Semesterplanung aus?",
        "answers": [{
            "id": 33,
            "type": "radiobutton",
            "task": "Wie viele Credits hast du im letzten Semester gemacht?",
            "options": ["weniger als 10", "10-20", "20-30", "30-40", "mehr als 40"]
        }, {
            "id": 34,
            "type": "radiobutton",
            "task": "Wie viele Credits planst du, nächstes Semester zu machen?",
            "options": ["weniger als 10", "10-20", "20-30", "30-40", "mehr als 40"]
        }, {
            "id": 35,
            "type": "radiobutton",
            "task": "Weißt du schon, welche Veranstaltungen, du im nächsten Semester besuchen willst?",
            "orientation": "inline",
            "options": ["ja", "nein", "teilweise"]
        }]
    }, {
        "id": 16,
        "questiontext": "StudIP",
        "answers": ["Im Folgenden geht es um die Nutzung von StudIP.", {
            "id": 36,
            "type": "radiobutton",
            "task": "Wie zufrieden bist du mit StudIP insgesamt?",
            "options": ["sehr unzufrieden", "eher unzufrieden", "eher zufrieden", "sehr zufrieden"]
        }]
    }, {
        "id": 17,
        "questiontext": "Wie viele deiner Veranstaltungen nutzen StudIP?",
        "answers": [{
            "id": 37,
            "type": "dropdown",
            "options": ["0-25%", "25-50%", "50-75%", "75-99%", "alle", "keine sinnvolle Angabe möglich"]
        }, {
            "id": 38,
            "type": "freetext",
            "task": "Wenn nicht alle StudIP nutzen, über welche Wege bekommst du Informationen zu den Veranstaltungen?",
            "rows": 4,
            "cols": 30
        }]
    }, {
        "id": 18,
        "questiontext": "Wie häufig benutzt du StudIP?",
        "answers": [{
            "id": 39,
            "type": "radiobutton",
            "options": ["seltener als einmal pro Monat", "mehrmals pro Monat", "mehrmals pro Woche", "täglich"]
        }]
    }, {
        "id": 19,
        "questiontext": "Wie oft nutzt du folgende Funktionen im StudIP?",
        "answers": [{
            "id": 40,
            "type": "radiobutton-matrix",
            "labels": ["täglich", "mehrmals pro Woche", "mehrmals pro Monat", "seltener als monatlich", "noch nie benutzt"],
            "options": ["Startseite", "Veranstaltungsseite", "  > eigene Farbgruppierung", "  > alles als gelesen markieren", "  > verschiedene Ansichten (z.B. nach Dozenten, ...)", "  > neue Studiengruppe anlegen", "Nachrichten-Box", "Community", "Planer", "  > Stundenplan", "  > Terminkalender", "  > ausblenden von Veranstaltungen", "  > Druckansicht", "  > Termine exportieren", "  > Termine importieren", "Suche", "  > Veranstaltungssuche", "  > Archiv", "  > Personensuche", "  > Einrichtungssuche", "  > Ressourcensuche", "Tools", "  > Fragebögen", "  > Evaluationen", "  > Literatur", "  > Lernmodule", "  > Export", "Schwarzes Brett"]
        }, {
            "id": 41,
            "type": "radiobutton",
            "task": "Kennst du das DoIT-System innerhalb des StudIPs?",
            "orientation": "inline",
            "options": ["ja", "nein"]
        }]
    }, {
        "id": 20,
        "questiontext": "Nutzt du die verschiedenen Ecampusdienste nur über Ecampus oder greifst du auf manche direkt zu? (z.B. über studip.uni-goettingen.de)",
        "answers": [{
            "id": 42,
            "type": "radiobutton",
            "task": "Gemeint sind Dienste wie StudIP, Email, UniVZ, Mensa Essensplan, FlexNow.",
            "options": ["über Ecampus", "teilweise direkt", "überwiegend direkt", "andere Wege"]
        }]
    }, {
        "id": 21,
        "questiontext": "Auslandssemester",
        "answers": [{
            "id": 43,
            "type": "radiobutton",
            "task": "Hast du schonmal ein Auslandsemester/studienrelevanten Auslandsaufenthalt gemacht?",
            "orientation": "inline",
            "options": ["ja", "nein"]
        }, {
            "id": 46,
            "type": "checkbox",
            "task": "Wenn du noch keinen Auslandsaufenthalt gemacht hast, warum nicht?",
            "options": ["Kein Interesse", "Ich würde gerne, aber ich weiß nicht, an wen ich mich wenden kann.", "Ist schon geplant", {
                "id": 45,
                "type": "compose",
                "options": ["Andere: ", {"id": 44, "type": "freetext", "rows": 1, "cols": 20}]
            }]
        }, {
            "id": 47,
            "type": "radiobutton",
            "task": "Gibt es in deinem Studiengang eine Informationsveranstaltung zu Auslandaufenthalten?",
            "orientation": "inline",
            "options": ["ja", "nein", "weiß ich nicht"]
        }, {
            "id": 48,
            "type": "radiobutton",
            "task": "Hast du diese schon einmal besucht?",
            "orientation": "inline",
            "options": ["ja", "nein", "es gibt keine solche Veranstaltung"]
        }]
    }, {
        "id": 22,
        "questiontext": "Hast du die Möglichkeit deine Abschlussarbeit in Kooperation mit einer Firma zu schreiben?",
        "answers": [{
            "id": 49,
            "type": "radiobutton",
            "task": "Wähle Zutreffendes aus.",
            "orientation": "inline",
            "options": ["ja", "ja, aber ich wüsste nicht wie", " weiß ich nicht", "nein"]
        }]
    }, {
        "id": 23,
        "questiontext": "Wie nimmst du die Bewerbung von Kolloquien wahr?",
        "answers": [{
            "id": 50,
            "type": "radiobutton-matrix",
            "task": "Gib an, wie sehr diese Aussagen für dich zutreffen.",
            "labels": ["stimme überhaupt nicht zu", "stimme nicht zu", "stimme zu", "stimme voll und ganz zu"],
            "options": ["Werbung für Kolloquien erreicht mich immer.", "Werbung für Kolloquien empfinde ich nicht als Spam.", "Ich lese mir durch, worüber ein Kolloquium ist.", "Wenn ich Zeit habe, gehe ich immer zu Kolloquien.", "Die Bewerbung für Kolloquien ist sehr ansprechend gemacht."]
        }]
    }, {
        "id": 24,
        "questiontext": "Wie nimmst du das Studienbüro (u.A. Studienberatung) an deiner Fakultät wahr?",
        "answers": [{
            "id": 53,
            "type": "checkbox",
            "task": "Gib an, welche Angebote es gibt.",
            "options": ["Master-Informationsveranstaltung", "Informationsveranstaltung zum Nebenfach/etc.", "Auslandssemester-Information", "Website des Studienbüros", "Informationen zu freien HiWi-Stellen", {
                "id": 52,
                "type": "compose",
                "options": ["Andere: ", {"id": 51, "type": "freetext", "rows": 1, "cols": 30}]
            }]
        }, {
            "id": 54,
            "type": "checkbox-matrix",
            "task": "Welche Kommunikationskanäle nutzt das Studienbüro? Wähle Zutreffendes aus.",
            "labels": ["wird genutzt", "wird nicht genutzt", "Informationen erreichen mich", "sollte mehr genutzt werden"],
            "options": ["Email", "Aushänge", "Flyer", "Ankündigungen in Lehrveranstaltungen", "Fachschaft", "Homepage", "Blog", "Andere"]
        }]
    }, {
        "id": 25,
        "questiontext": "Studienqualität",
        "answers": ["Dies ist die letzte Frage. Was würde am meisten dazu beitragen, deine Studienqualität zu erhöhen?", {
            "id": 55,
            "type": "freetext",
            "rows": 5,
            "cols": 30
        }]
    }]
};

describe(' REST API tests', function () {
    it('post new questionnaire with valid admin token', async function () {
        return postQuestionnaire(exampleQuestionnaire, AdminToken)
            .expect('status', 201)
            .expect('bodyContains', /[a-zA-Z0-9]{30}/);
    });

    it('post new questionnaire without valid admin token', async function () {
        return postQuestionnaire(exampleQuestionnaire, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
            .expect('status', 403);
    });

    it('post new questionnaire without valid admin token', async function () {
        return postQuestionnaire(exampleQuestionnaire, '12@%^')
            .expect('status', 403);
    });

    it('add new user', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        return getAuthenticationToken(qid)
            .expect('status', 201)
            .expect('bodyContains', /[a-zA-Z0-9]{30}/);
    });

    it('add new user without valid questionnaire id', async function () {
        return getAuthenticationToken('random')
            .expect('status', 404);
    });

    it('get default answerset for a questionnaire', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        return getAnswers(qid, uid)
            .expect('status', 200);
    });

    it('post questionnaire and get it back', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        //return getQuestionnaire(qid, uid)
        //     .expect('status', 200)
        //.expect('json', exampleQuestionnaire);
    });

    it('get a questionnaire that does not exist', async function () {
        return getQuestionnaire('randomqidxrandomqidxrandomqidx', 'randomuidxrandomuidxrandomuidx')
            .expect('status', 404);
    });

    it('post questionnaire and get it back without valid user', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        return getQuestionnaire(qid, 'randomuidxrandomuidxrandomuidx')
            .expect('status', 404);
        //.expect('json', exampleQuestionnaire);
    });

    it('get default answerset and send it back', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        let answerset = (await getAnswers(qid, uid)).body.answerset;
        let response = await axios.post(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}/answers?uid=${uid}`, {"answerset": answerset});
        return expect(response.status).toBe(200);
    });

    it('get progressbar type', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        return getProgressbarType(qid, uid)
            .expect('status', 200);
    });

    //testcase is failing due to incoomplete implemeting of api call and/or inconstistent to
    // frontend switch statement
    it('progress bars uniform distributed', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid;
        let counts = [0, 0, 0, 0, 0];
        let progressbarType;
        for (let i = 0; i < 500; i++) {
            uid = (await getAuthenticationToken(qid)).body;
            progressbarType = (await getProgressbarType(qid, uid)).body;
            switch (JSON.parse(progressbarType)['progressbarType']) {
                case "none":
                    counts[0]++;
                    break;
                case "linear":
                    counts[1]++;
                    break;
                case "slow-to-fast":
                    counts[2]++;
                    break;
                case "fast-to-slow":
                    counts[3]++;
                    break;
                case "smart":
                    counts[4]++;
                    break;
            }
        }
        let allEqual = false;
        if (counts[0] === counts[1] && counts[0] === counts[2] && counts[0] === counts[3]
            && counts[0] === counts[4]) {
            allEqual = true;
        }
        expect(allEqual).toBeTruthy();
        expect(counts[0]).toEqual(100);
    });


    it('if post a valid answerset to a non-existing user id an error should occur', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        let answerset = await getAnswers(qid, uid).body;

        let newUid;
        do {
            newUid = Array(UID_LENGTH).fill(0).map(() => ALPHABET[Math.floor(Math.random() * ALPHABET.length)]).join('');
        } while (uid === newUid);
        return frisby.post(`https://localhost:${HTTPSPORT}/api/questionnaires/${qid}/answers?uid=${newUid}`, answerset).expect('status', 404);
    });
});