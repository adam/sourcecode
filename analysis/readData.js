const fs = require('fs');
const path = require('path');

// tested
// read file (relatively to the path of the script)
function readData(filename) {
    return JSON.parse(fs.readFileSync(path.join(__dirname, filename)));
}

// tested
function mergeAccessTimeWithAnswers(accessTimeFile, answerFile) {
    let accessTimes = readData(accessTimeFile);
    let answers = readData(answerFile);
    let results = {}; // inside here will be questionnaires
    for (let accessTime of accessTimes) {
        results[`${accessTime.qid}${accessTime.uid}`] = {
            qid: accessTime.qid,
            uid: accessTime.uid,
            start: accessTime.start,
            end: accessTime.end
        };
    }
    for (let answer of answers) {
        if (results[`${answer.qid}${answer.uid}`] === undefined)
            throw new Error('answers contains data without time record');
        results[`${answer.qid}${answer.uid}`].answerset = answer.answerset;
        results[`${answer.qid}${answer.uid}`].progressbarType = answer.progressbarType;
    }
    let res = [];
    for (let key in results) {
        if (results.hasOwnProperty(key)) // necessary
            res.push(results[key]);
    }
    return res;
}

// call with output of mergeAccessTimeWithAnswers
function finishedSurvey(dataSet) {
    return mergeAccessTimeWithAnswers('./sample-accessTime.json', './sample-answers.json').map((data) => {
        return {
            qid: data.qid,
            uid: data.uid,
            finished: data.answerset.filter((o) => o.id === 55).length > 0 // 55 ist the last question that has to be answered. is there any better information about survey completion?
        };
    });
}

//console.log(JSON.stringify(mergeAccessTimeWithAnswers('./sample-accessTime.json',
// './sample-answers.json')));
console.log(finishedSurvey(mergeAccessTimeWithAnswers('./sample-accessTime.json', './sample-answers.json')));
exports.readData = readData;
